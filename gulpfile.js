/*
Gulp
    $ npm i -g gulp
    $ npm i -D gulp

Errors log
    $ npm i -D gulp-plumber

Server & LiveReload
    npm i -D gulp-connect

ejs
    npm i -D gulp-ejs

Sass
    Install Ruby : http://rubyinstaller.org/downloads/
    Install Sass : $ gem install sass
    Install compass : $ npm i -D gulp-compass

js hint
    $ npm i -D gulp-jshint
    $ npm i -D jshint-stylish

image-min
    $ npm i -D gulp-imagemin
    $ npm i -D imagemin-pngquant

*/

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    compass = require('gulp-compass'),
    connect = require('gulp-connect'),
    ejs = require('gulp-ejs'),
    jshint = require('gulp-jshint'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

/* Path */
var src = './public/src';
var dist = './public/dist';

/* Server */
gulp.task('serve', function() {
	connect.server({
		root: dist,
		port: 8010,
		livereload: true,
        open: {
			browser: 'chrome'
		}
	});
});

/* SASS -> SCSS */
gulp.task('compass', function() {
	gulp
        .src(src+'/scss/**/*.{scss,sass}')
    	.pipe( plumber({
    		errorHandler: function (error) {
    			console.log(error.message);
    			this.emit('end');
    		}
    	}) )
    	.pipe(compass({
    		css: src+'/css',
    		sass: src+'/scss',
    		style: 'compact' // nested, expaned, compact, compressed
    	}))
    	.pipe( gulp.dest(dist+'/css') )
        .pipe(connect.reload())
});

/* ejs -> HTML */
gulp.task('ejs', function() {
	gulp
        .src([src+'/ejs/**/*.ejs',  '!' + src+'/ejs/**/_*.ejs'])
        .pipe(ejs({}, {ext: '.html'}))
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
    	.pipe(gulp.dest(dist+'/html'))
        .pipe(connect.reload())
});

/* js hint */
gulp.task('js:hint', function () {
    gulp
        .src([src+'/js/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(gulp.dest(dist+'/js'))
        .pipe(connect.reload())
});

/* image */
gulp.task('imgmin', function() {
	gulp
        .src([src+'/images/**/*'])
		.pipe(imagemin({
			progressive: true,
            interlaced:true,
			use: [pngquant()]
		}))
		.pipe(gulp.dest(dist+'/images'))
        .pipe(connect.reload())
});

//Watch task
gulp.task('watch',function() {
    gulp.watch(src+'/ejs/**/*.ejs',['ejs']);
    gulp.watch(src+'/scss/**/*.{scss,sass}',['compass']);
    gulp.watch(src+'/js/**/*.js',['js:hint']);
    gulp.watch(src+'/images/**/*',['imgmin']);
});

gulp.task('default', ['serve','compass','ejs','js:hint','imgmin','watch']);
