$(document).ready(function(){
	$(window).load(function(){
		$(window).trigger('resize');

		/* 사은품 슬라이드 */
		var freeNum = $('.freebies__item').length
		if(freeNum >= 3){
			var swiper5 = new Swiper('.freebies__list',{
				pagination: '.swiper-pagination',
				slidesPerView: 2,
				nextButton: '#freebies__btn-next',
				prevButton: '#freebies__btn-prev',
				buttonDisabledClass : 'is-hidden'
			});
			$('.freebies__slide-btn').show();
		}
		/*풀샥특가 슬라이드*/
		$('.sale__list').slick({
			centerMode: true,
			centerPadding: '120px',
			slidesToShow: 1,
			prevArrow: '#main__btn-prev',
			nextArrow: '#main__btn-next',
			responsive: [
		    {
		      breakpoint: 460,
		      settings: {
		        centerMode: true,
		        centerPadding: '90px',
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 360,
		      settings: {
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 1
		      }
		    }
		  ]
		});
	})
	var saleTotalNum = $('.sale__item').length;
	$('.main__sale-number').html('('+1+'/'+saleTotalNum+')')
	$('.sale__list').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  $('.main__sale-number').html('('+(nextSlide+1)+'/'+saleTotalNum+')')
	});
	/* 사은품 슬라이드 끝*/

	var height = $(window).height();
	//메뉴블랙영역잡기
	$(window).resize(function(){
		var winH = $('body').height();
		var footerH = $('.footer').height()+61;/*푸터픽스드 시킬떄 50추가*/
		$('.white, .black').height(winH+footerH);
		$('body').css('padding-bottom',footerH)
	});
	//메뉴
	$(function(){
		$('.gnb__1depth-list a').on('click', function(){
			var p = $(this).parents('.gnb__1depth-list');
			if($(p).hasClass('on')){
				$(p).removeClass('on').next().hide();
			} else {
				$('.gnb__1depth .on').next().hide();
				$('.gnb__1depth-list').removeClass('on')
				$(p).addClass('on').next().show();
			}
		});
		$('.header__menu-btn').on('click', function(){
			var m = $(this);
			$(m).addClass('on').parents('body').find('.white, .gnb').show();
		});
		$('#gnbClose').on('click', function(e){
			e.preventDefault();
			var m = $(this);
			$(m).removeClass('on').parents('body').find('.white, .gnb').hide();
		});
	});



	//tab
	$(function() {
		var tab = $("[data-tab='true']");
		var hash = window.location.hash;
		var tt= [];

		$(tab).find("a").each(function(idx){
			var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
			tt.push(t);

			if (hash) {
				sele($(tab).find("a[href="+hash+"]"));
				for (var i in tt) {
					$("#"+tt[i]).removeClass("is-active");
				}
				$(hash).addClass("is-active");
			}

			$(this).on("click", function(e){
				if (this.href.match(/#([^ ]*)/g)) {
					e.preventDefault();
					if (!$(this).parent().hasClass("is-active")) window.location.hash = ($(this).attr("href"));
					sele($(this));
					for (var i in tt) {
						$("#"+tt[i]).removeClass("is-active");
					}
					$("#"+t).addClass("is-active");
				}
				$('.tab-v2__item').removeClass('clear')
				$('.tab-v2__item.is-active').prev().addClass('clear');
			});



		})
		if ($(tab).hasClass("tab-v1")) {
			$(tab).find("[class$=list]").on("click", function(){
				//console.log($('body').attr('data-mobile'));
				if ($('body').attr('data-mobile') == 'true'){
					if ($(this).hasClass("js-open-m")) {
						$(this).removeClass("js-open-m");
					} else {
						$(this).addClass("js-open-m");
					}
				}
			});
			$(window).resize(function(){
				if ($('body').attr('data-mobile') == 'false') $(".tab-v1").removeClass("js-open-m");
			});
		}

		function sele(el) {
			$(el).parent().addClass("is-active").siblings().removeClass("is-active");
		}

	});
	$(window).load(function(){
		var selectBox1 = $(".js-default").selectBoxIt({autoWidth:false});
		var selectBox2 = $(".js-bank").selectBoxIt({defaultText:'<span style="color:#999;">은행을 선택해주세요</span>', autoWidth:false});
	});

	//리스트 형태바꾸기
	var cnt = 1
	$('#listbtn').on('click',function(){

		if(cnt == 1){
			$(this).addClass('is-active')
			$('.prod_info').addClass('type-wide');
			$('.main__content-item').css('width','100%');
			cnt = 0
		}else{
			$(this).removeClass('is-active')
			$('.prod_info').removeClass('type-wide');
			$('.main__content-item').css('width','50%');
			cnt = 1;
		}
	});

	//이미지 슬라이드
	var swiper1 = new Swiper('.main__slide', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '#main__btn-next',
		prevButton: '#main__btn-prev',
		loop: true
	});
	var swiper2 = new Swiper('.prod__slide', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '#main__btn-next',
		prevButton: '#main__btn-prev',
		loop: true
	});
	var swiper3 = new Swiper('.recipe__list', {
		nextButton: '#recipe__btn-next',
		prevButton: '#recipe__btn-prev',
		loop: true
	});
	var swiper4 = new Swiper('.brand__list', {
		nextButton: '#brand__btn-next',
		prevButton: '#brand__btn-prev',
		loop: true
	});


	$(function(){
		$('.accordian1_item:first-child').addClass('on').find('.accordian1_item_body').css('display','block');

		$(".accordian1_item_header").on("click keypress",function(e){
			var $target = $(this).parent("li");
			if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
			if ( $target.is('.on')){
				$(this).next().slideDown('500');
				$target.siblings().find('.accordian1_item_body').slideUp('500');
			} else {
				$(this).next().slideUp('500');
			}
		});
	});

	$(function(){
		$('.accordian2_item:first-child').addClass('on').find('.accordian2_item_body').css('display','block');
		$(".accordian2_item_header").on("click keypress",function(e){
			var $target = $(this).parent("li");
			if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
			if ( $target.is('.on')){
				$(this).next().slideDown('500');
				$target.siblings().find('.accordian2_item_body').slideUp('500');
			} else {
				$(this).next().slideUp('500');
			}
		});
	});

	$(function(){
		$('.accordian3_item:first-child').addClass('on').find('.accordian3_item_body').css('display','block');
		$(".accordian3_item_header_wrap").on("click keypress",function(e){
			var $target = $(this).parents("li");
			if ((e.keyCode == 13)||(e.type == 'click'))$(this).parents("li").toggleClass("on").siblings().removeClass("on");
			if ( $target.is('.on')){
				$(this).parents('.accordian3_item_header').next().slideDown('500');
				$target.siblings().find('.accordian3_item_body').slideUp('500');
			} else {
				$(this).parents('.accordian3_item_header').next().slideUp('500');
			}
		});
	});

	$(function(){
		$('.accordian4_item:first-child').addClass('on').find('.accordian4_item_body').css('display','block');
		$(".accordian4_item_header").on("click keypress",function(e){
			var $target = $(this).parent("li");
			if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
			if ( $target.is('.on')){
				$(this).next().stop().slideDown('500');
				$target.siblings().find('.accordian4_item_body').slideUp('500');
			} else {
				$(this).next().stop().slideUp('500');
			}
		});
	});


	// $(function() {
	//   $("body").swipe( {
	// 	//Generic swipe handler for all directions
	// 	swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
	// 	  if(direction == 'down'){
	// 		  $('.footer-fix').hide();
	// 		  $('.footer').removeClass('is-active');
	// 	  }else if(direction == 'up'){
	// 		  $('.footer-fix').show()
	// 		  $('.footer').addClass('is-active');
	// 	  }
	// 	},
	// 	//Default is 75px, set to 0 for demo so any distance triggers swipe
	// 	 threshold:0
	//   });
	// });

	// 수량 증가 감소
	//counter
	$('.counter_minus').click(function(){
		CntVal = $(this).next('span').children('input').val();
		CntVal--;
		if (CntVal < 0)
		{
			CntVal = 0;
		}
		$(this).next('span').children('input').val(CntVal);
	});

	$('.counter_plus').click(function(){
		CntVal = $(this).prev('span').children('input').val();
		CntVal++;
		$(this).prev('span').children('input').val(CntVal);
	});

	/*selectbox*/
	$(document).ready(function(){
	  $(".select3__header").click(function(){
	    $(this).parent().toggleClass("showMenu");
	      $(".select3__menu > li").click(function(){
	        $(this).parents('.select3').find('.select3__header > p').text($(this).text());
	        $(this).parents('.select3').removeClass("showMenu");
	      });
	  });

	});
	//구매하기 오픈
	$('.buytab__open').on('click',function(e){
		e.preventDefault();
		$(this).parents('.buytab').toggleClass('is-active');
	});

	$('.prod_set-btn').on('click',function(e){
		 var lineH = $(this).parents('.detail__content-set')
		e.preventDefault();
		lineH.toggleClass('is-active');
	})
	$('.js-set').on('click',function(e){
		var cItem = $(this).parents('.content-set__item')
		e.preventDefault();
		$('.content-set__list').prepend(cItem);
		$('.detail__content-set').removeClass('is-active')
	})
	//탑배너
	$('.js-top-close').on('click',function(e){
		e.preventDefault();
		$(this).parent().addClass('is-hidden');
	});
	//스크롤 탑
	$(window).scroll(function(){
		if($(this).scrollTop()>0){
			$('.top__btn').fadeIn();
			//$('.footer-fix').addClass('is-active')
		}else{
			$('.top__btn').fadeOut();
			//$('.footer-fix').removeClass('is-active')
		}
	});

	$('#topBtn').click(function(){
		$('html,body').animate({'scrollTop':'0'},300)
	});
	//레이어팝업
	$('.js-popup-open').on('click',function(e){
		e.preventDefault();
		$('.layer-popup, .black').show();
	});
	$('.js-popup-close').on('click',function(e){
		e.preventDefault();
		$('.layer-popup, .black').hide();
	});
	$('.js-popup-open2').on('click',function(e){
		e.preventDefault();
		$('.layer-popup.type-address, .black').show();
	});
	$('.js-popup-open3').on('click',function(e){
		e.preventDefault();
		$('.layer-popup.type-calendar, .black').show();
	});
	$('.js-popup-open4').on('click',function(e){
		e.preventDefault();
		$('.layer-popup.type-custom, .black').show();
	});
	//장바구니 할인금액
	$('.payment__btn').on('click',function(e){
		e.preventDefault();
		$(this).parents('.payment__item').toggleClass('on');

	});
	//장바구니
	var cnt2 = 1
	$('.js-order').on('click',function(e){
		e.preventDefault();

		if(cnt2 == 1){
			$(this).addClass('is-active');
			$(this).parents('.order__header').next().addClass('is-active');
			cnt2 = 0;
		}else{
			$(this).removeClass('is-active');
			$(this).parents('.order__header').next().removeClass('is-active');
			cnt2 = 1;
		}
	});

	//배송지 정보
	$('.js-delivery').on('click',function(e){
		e.preventDefault();
		$(this).addClass('is-active').siblings().removeClass('is-active')
		var menuName = $(this).attr('data-name')
		if(menuName == 'default'){
			$(this).parents('.order__address-box').find('.js-default').addClass('is-active').siblings().removeClass('is-active');
		}else if(menuName == 'direct'){
			$(this).parents('.order__address-box').find('.js-direct').addClass('is-active').siblings().removeClass('is-active');
		}
	});

	//최근배송지
	$('.js-popup-delivery').on('click',function(e){
		e.preventDefault();
		$(this).addClass('is-active').siblings().removeClass('is-active')
		var menuName2 = $(this).attr('data-name')
		if(menuName2 == 'basics'){
			$(this).parents('.layer-popup').find('.js-basics').addClass('is-active').siblings().removeClass('is-active');
		}else if(menuName2 == 'recently'){
			$(this).parents('.layer-popup').find('.js-recently').addClass('is-active').siblings().removeClass('is-active');
		}
	});

	//결제수단
	$('.order__means-link').on('click',function(e){
		e.preventDefault();
		$('.order__means-link').removeClass('is-active');
		$(this).addClass('is-active');
		var menuName3 = $(this).attr('data-name')
		if(menuName3 == 'case1'){
			$(this).parents('.order__body').find('.js-case1').addClass('is-active').siblings().removeClass('is-active');
		}else if(menuName3 == 'case2'){
			$(this).parents('.order__body').find('.js-case2').addClass('is-active').siblings().removeClass('is-active');
		}else if(menuName3 == 'case3'){
			$(this).parents('.order__body').find('.js-case3').addClass('is-active').siblings().removeClass('is-active');
		}
		// e.preventDefault();
		// nowCnt = $(this).parent();
		// var listCnt = $('.order__means-item').index(nowCnt)
		// $('.order__means-link').removeClass('is-active');
		// $(this).addClass('is-active');
		// $('.order__means-case').eq(listCnt).addClass('is-active').siblings().removeClass('is-active');
	});


	/* 작업본 */
	/* 후기리스트 */
	$(function(){
		prodPost(),
		uiSimpleToggle()
	});

	var prodPost = function() {
		var t = 300,
			i = $(".prod_post"),
			e = i.find(".post_title a, .post_title button");
		$answer = i.find(".post_body"), e.on("click", function(i) {
			$(this).parents("li").is(".active") ? ($answer.stop(!0, !0).slideUp(t), $answer.parent("li").removeClass("active")) : ($(this).parent().next($answer).stop(!0, !0).slideDown(t).parent("li").siblings().find($answer).stop(!0, !0).slideUp(t), $(this).parents("li").addClass("active").siblings().removeClass("active")), i.preventDefault()
		});
	},

	uiSimpleToggle = function() {
		var t = $("ul.uiToggle"),
			i = t.find("button.postbutton, a.postbutton");
		i.on("click", function() {
			$(this).parent("li").is(".active") ? $(this).parent("li").removeClass("active") : $(this).parent("li").addClass("active").siblings().removeClass("active")
		});
	};

	/* 환경설정 */
	$('.my_menu_set li .toggle').click(function(){
		if($(this).hasClass('on')){
     		$(this).removeClass('on');
		}
		else {
			$(this).addClass('on');
		}
	});

	/* 별 */
	$('.star_control .star1').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star1');
		}
	});
	$('.star_control .star2').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star2');
		}
	});
	$('.star_control .star3').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star3');
		}
	});
	$('.star_control .star4').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star4');
		}
	});
	$('.star_control .star5').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star5');
		}
	});


});
